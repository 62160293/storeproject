/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.panuwat.storeproject;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.User;

/**
 *
 * @author AKYROS
 */
public class TestInsertUser {
    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "INSERT INTO customer (name,tel) VALUES (?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            User users = new User(-1,"Kanmati konichiwa",20);
            stmt.setString(1,users.getName());
            stmt.setString(2,users.getTel());
            stmt.setString(1,users.getName());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            int id = -1;
            if (result.next()) {
                id = result.getInt(1);
            }
            System.out.println("Affect row " + row + " id " + id);

        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
    }
}
